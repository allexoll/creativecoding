function setup() {
   createCanvas(windowWidth, windowHeight);
}

var speed = 50;
var phase = 0.0;
function draw() {
   var x = sin(phase + 1.0/speed);
   var y = cos(phase + 1.0/speed);
   phase = phase + 1.0/speed;
   if(phase > 2*PI)
   {
      phase = 0.0;
   }
   var colorM = map(x, -1,1, 0,255);
   //print(colorM);
   var weightM = map(x, 1, -1, 0,30);
   strokeWeight(weightM);
   background(colorM);
   stroke(255);
   line(0,0,width,height);
   line(width,0,0,height);
   fill(255 - colorM);
   ellipse(width/2 + (width/5)*x, height/2 + (height/5)*y, 30, 30);


}

function windowResized()
{
   print("resize");
   resizeCanvas(windowWidth, windowHeight);
}
