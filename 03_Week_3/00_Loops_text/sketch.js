var words = ["this", "was", "in", "the", "beginning", "before"];
var out = "" ;
var offset = 0;

function setup() {
   createCanvas(windowWidth, windowHeight);
   print(words);
   print(words.length);
}

function draw() {

}

function mousePressed() {
   shuffle(words, true);
   out = "";
   for(var i = 0; i < words.length; i++)
   {
      out += words[i] + " ";
   }
   //background(255);
   textSize(36);
   text(out, 100, 100 + offset);
   offset += 30;
}
